
-- ----------------------------
-- Table structure for `user`;
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`;(
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(45) DEFAULT NULL,
  `PASSWORD` varchar(20) DEFAULT NULL,
  `PHONE_NUMBER` varchar(45) DEFAULT NULL,
  `WECHAT_ID` varchar(32) DEFAULT NULL,
  `WECHAT_NAME` varchar(45) DEFAULT NULL,
  `GENDER` char(1) DEFAULT NULL,
  `BIRTHDAY` date DEFAULT NULL,
  `EMAIL` varchar(45) DEFAULT NULL,
  `CREATE_DT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `user_group`
-- ----------------------------
DROP TABLE IF EXISTS `user_group`
CREATE TABLE `user_group`(
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;