package wang.lovedota.blog.model.group;

import java.util.List;

import com.google.common.collect.Lists;

import wang.lovedota.blog.model.user.User;

public class UserGroup {
	
	private Long id;
	
	private String name;
	
	private List<User> users=Lists.newArrayList();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	public void add(User user){
		users.add(user);
	}
}
