package wang.lovedota.blog.utils;

import java.io.Closeable;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

public class IOCloseUtils {

	public static void close(InputStream is) {

		if (null == is) {
			return;
		}

		try {
			is.close();
			PssLogFactory.getDebugLog().debug("close input stream");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static void close(InputStreamReader is) {

		if (null == is) {
			return;
		}

		try {
			is.close();
			PssLogFactory.getDebugLog().debug("close input stream");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static void close(OutputStream os) {

		if (null == os) {
			return;
		}

		try {
			os.flush();
			os.close();
			PssLogFactory.getDebugLog().debug("close output stream");
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
				os.close();
			} catch (Exception e) {

				PssLogFactory.getDebugLog().debug("close output stream");

			}
		}

	}

	private static void closeQuiet(Closeable single) {
		if (null != single) {
			try {
				single.close();
			} catch (Exception e) {

				PssLogFactory.getDebugLog().debug("close  stream");
			}
		}
	}

	public static void closeBatch(Closeable... cls) {

		for (Closeable os : cls) {
			try {
				closeQuiet(os);
			} catch (Exception ioe) {
				PssLogFactory.getDebugLog().debug("close output stream");
			}
		}

	}

}
