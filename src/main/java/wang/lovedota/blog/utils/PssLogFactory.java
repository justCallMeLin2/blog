package wang.lovedota.blog.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PssLogFactory {

	private static final Logger ACCESS = LoggerFactory.getLogger("ACCESS");

	private static final Logger BUSINESS = LoggerFactory.getLogger("BUSINESS");

	private static final Logger ERROR = LoggerFactory.getLogger("ERROR");

	private static final Logger DEBUG = LoggerFactory.getLogger("DEBUG");

	private static final Logger JOB = LoggerFactory.getLogger("JOB");

	public static Logger getLog(Class<?> clazz) {
		return LoggerFactory.getLogger(clazz);
	}

	public static Logger getAccessLog() {
		return ACCESS;
	}

	public static Logger getBusinessLog() {
		return BUSINESS;
	}

	public static Logger getErrorLog() {
		return ERROR;
	}

	public static Logger getDebugLog() {
		return DEBUG;
	}

	public static Logger getJobLog() {
		return JOB;
	}
}
