﻿package wang.lovedota.blog;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @author lin
 *spring-boot 运行的入口
 *只需要启动main函数即可---
 */
@SpringBootApplication		// same as @Configuration @EnableAutoConfiguration @ComponentScan
public class Application {
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
       // SpringApplication application = new SpringApplication(Application.class);  
        //application.setBannerMode(bannerMode);
	}
}
